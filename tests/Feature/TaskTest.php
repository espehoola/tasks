<?php

namespace Tests\Feature;

use App\Models\Image;
use App\Models\Task;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class TaskTest extends TestCase
{
    public string $url = '/v1/task';

    public function testCreateTask(): void
    {
        Storage::fake('public');
        $file = UploadedFile::fake()->image('test.jpg');

        $data = [
            'name' => 'Test',
            'description' => 'qwerty12',
            'price' => 100,
            'images' => [$file],
        ];

        $response = $this->post($this->url, $data);

        $response->assertStatus(201);
        $response->assertJsonFragment($data);
    }

    public function testViewTask()
    {
        $task = Task::factory()->has(Image::factory()->count(3))->create();

        $response = $this->get($this->url . '/' . $task->id);

        $response->assertStatus(200);
        $response->assertJsonStructure(['name', 'description', 'price', 'image']);
    }

    public function testIndexTask()
    {
        Task::factory()->has(Image::factory()->count(3))->create();

        $response = $this->get($this->url, ['order' => 'created_at', 'direction' => 'desc']);

        $response->assertOk();
        $response->assertJsonStructure(['data', 'current_page', 'per_page', 'total']);
    }
}
