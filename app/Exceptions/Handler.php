<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Config;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function render($request, \Throwable $exception)
    {
        $response = [];

        $status = $exception->status ?? 500;
        if ($status === 422) {
            $response['errors'] = $exception->errors();
        }
        $response['message'] = $exception->getMessage();
        if (Config::get('app.debug')) {
            $response['trace'] = $exception->getTrace();
        }

        return response()->json($response, $status);
    }
}
