<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class TaskIndexRequest extends NotAuthorizedRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order' => [
                'string',
                Rule::in(['created_at', 'price']),
            ],
            'direction' => [
                'string',
                Rule::in(['asc', 'desc']),
            ],
        ];
    }
}
