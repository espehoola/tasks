<?php

namespace App\Http\Requests;

class TaskStoreRequest extends NotAuthorizedRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:200|unique:tasks,name',
            'description' => 'required|string|max:1000',
            'price' => 'required|numeric',
            'images' => 'required|array',
            'images.*' => 'image',
        ];
    }
}
