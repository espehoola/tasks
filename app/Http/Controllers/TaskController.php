<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskIndexRequest;
use App\Http\Requests\TaskStoreRequest;
use App\Models\Image;
use App\Models\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskController extends Controller
{
    public function index(TaskIndexRequest $request)
    {
        $query = Task::query()
            ->when($request->order, function (Builder $query) use ($request) {
                return $query->orderBy($request->order, $request->get('direction', 'asc'));
            })
        ->with('image');

        return $query->paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        throw new NotFoundHttpException();
    }

    public function store(TaskStoreRequest $request)
    {
        $task = new Task($request->all());
        $task->save();

        if ($request->hasfile('images')) {
            foreach ($request->file('images') as $file) {
                $name = uniqid() . '.' . $file->extension();
                $file->move(public_path(Config::get('app.upload_dir')), $name);
                $image = new Image();
                $image->path = Config::get('app.upload_dir') . '/' . $name;
                $image->size = $file->getSize();
                $task->image()->save($image);
            }
        }

        return $task;
    }

    /**
     * Display the specified resource.
     * @param Task $task
     */
    public function show(Task $task)
    {
        return $task->load('image');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
