DO $$
BEGIN
  CREATE ROLE tasks_user WITH LOGIN PASSWORD 'superpassword';
  EXCEPTION WHEN OTHERS THEN
  RAISE NOTICE 'Role is already exists';
END
$$;
CREATE DATABASE tasks_db OWNER tasks_user;
