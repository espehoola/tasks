#!/bin/bash
cp .env.example .env
docker-compose up -d --build
docker-compose exec php composer install
docker-compose exec php php artisan migrate
docker-compose exec php php artisan key:generate
